using UnityEngine;
using System.Collections;

public class GameControler : MonoBehaviour {
    public GameObject life1, life2, life3;
    [HideInInspector]
    public int counter = 0;
    float startTime, deltaT, timer;
	// Update is called once per frame
    void Start()
    {
        startTime = Time.time;
    }

    void Update()
    {
        timer = Time.time;
        deltaT = timer - startTime;
    }

    public void hit()
    {
        if(deltaT > 2)
        {
            if (counter == 0)
            {
                Destroy(life1);
            }
            else if (counter == 1)
            {
                Destroy(life2);
            }
            else if (counter == 2)
            {
                Destroy(life3);
                Application.LoadLevel("lose");
            }
            counter++;

            startTime = Time.time;
        }
    }
}
