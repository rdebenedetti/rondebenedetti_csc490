using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour {

    public float speed = 5;

    private CharacterMotor cm;
    private Square sqBody, sqHead, sqLegs;

    private int facing = 1;//1 for right, -1 for left

	void Start () {
         cm = GetComponent<CharacterMotor>();
         sqBody = GameObject.Find("body").GetComponent<Square>();
         sqHead = GameObject.Find("Head").GetComponent<Square>();
         sqLegs = GameObject.Find("leg1").GetComponent<Square>();
        
	}
	
	void Update () {
        cm.targetVelocity.x = Input.GetAxis("Horizontal") * speed;
       
        if (Input.GetAxis("Horizontal") > 0)
        {
            if (facing != 1)//direction changed
            {
                sqBody.flip = !sqBody.flip;
                sqBody.rotate = !sqBody.rotate;
                sqBody.UpdateMesh();

                sqHead.flip = !sqHead.flip;
                sqHead.rotate = !sqHead.rotate;
                sqHead.UpdateMesh();

                sqLegs.flip = !sqLegs.flip;
                sqLegs.rotate = !sqLegs.rotate;
                sqLegs.UpdateMesh();

                transform.rotation = Quaternion.AngleAxis(0, Vector3.up);
            }
            facing = 1;
        }
        else if(Input.GetAxis("Horizontal") < 0)
        {
            if (facing != -1)//direction changed
            {
                sqBody.flip = !sqBody.flip;
                sqBody.rotate = !sqBody.rotate;
                sqBody.UpdateMesh();

                sqHead.flip = !sqHead.flip;
                sqHead.rotate = !sqHead.rotate;
                sqHead.UpdateMesh();

                sqLegs.flip = !sqLegs.flip;
                sqLegs.rotate = !sqLegs.rotate;
                sqLegs.UpdateMesh();

                transform.rotation = Quaternion.AngleAxis(180, Vector3.up);
            }
            facing = -1;
        }
        cm.shouldJump = Input.GetButton("Jump");
	}
}
