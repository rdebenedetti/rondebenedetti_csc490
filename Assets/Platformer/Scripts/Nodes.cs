using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Nodes : MonoBehaviour
{
    public float brushWidth = 5.0f;
    public float depth = 1.0f;
    public List<Vector3> nodes;

    void Start()
    {
        nodes.Add(new Vector3(-9, -9, 0));
        nodes.Add(new Vector3(-8, -3, 0));
        nodes.Add(new Vector3(-6, -1, 0));
        nodes.Add(new Vector3(-5, -3, 0));
        nodes.Add(new Vector3(-0, -3, 0));
        nodes.Add(new Vector3(2, -3, 0));
        nodes.Add(new Vector3(4, 0, 0));
        nodes.Add(new Vector3(7, -13, 0));
        nodes.Add(new Vector3(8, -11, 0));
        nodes.Add(new Vector3(10, -2, 0));

        
    }
}
