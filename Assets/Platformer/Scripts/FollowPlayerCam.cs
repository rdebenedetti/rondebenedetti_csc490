using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class FollowPlayerCam : MonoBehaviour {

    public GameObject focus;
    public float offsetX, offsetY;
    
	void Update () {
        if (focus != null)
        {
            this.gameObject.transform.position = new Vector3(focus.transform.position.x + offsetX, focus.transform.position.y + offsetY, this.gameObject.transform.position.z);
        }
	}

}
