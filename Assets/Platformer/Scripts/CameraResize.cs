using UnityEngine;
using System.Collections;

public class CameraResize : MonoBehaviour {
    public GameObject topLeft, topRight, bottomLeft, bottomRight;
    public Camera mainCamera, resizeCamera;
	
	Vector3 tl, tr, bl, br;
	
	void Start () {
        tl = topLeft.transform.position;
		tr = topRight.transform.position;
		bl = bottomLeft.transform.position;
		br = bottomRight.transform.position;
		
		float x = mainCamera.WorldToViewportPoint(tl).x;
		float y = mainCamera.WorldToViewportPoint(bl).y;
		float width = mainCamera.WorldToViewportPoint(tr).x - mainCamera.WorldToViewportPoint(tl).x;
		float height = mainCamera.WorldToViewportPoint(tl).y - mainCamera.WorldToViewportPoint(bl).y; 
		
		resizeCamera.rect = new Rect(x, y, width, height);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
