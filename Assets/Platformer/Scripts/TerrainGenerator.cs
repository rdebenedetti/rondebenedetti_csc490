using UnityEngine;
using System.Collections;

public class TerrainGenerator : MonoBehaviour {
    
    Nodes n = null;

    public Camera cam;
    public float freqX = 10.0f;
    public float freqY = 10.0f;
    Vector2[] uvs = new Vector2[]
					{
			            new Vector2(0, 0),
                        new Vector2(1, 0),
                        new Vector2(1, 1),
                        new Vector2(0, 1)
					};
    Vector3[] norms = new Vector3[]{
                  Vector3.forward,
                  Vector3.forward,
                  Vector3.forward,
                  Vector3.back
            };

    int[] tris = new int[6] { 0, 1, 3, 1, 2, 3 };

	void Start () {
        n = GameObject.Find("tracklayer").GetComponent<Nodes>();
        drawAllNodes();
	}
	
	
	void Update () 
    {
        Vector3 leftPos = cam.WorldToViewportPoint(n.nodes[0]);
        if (leftPos.x > 0)
        {
            leftAdd(new Vector3(n.nodes[0].x - Random.Range(1, freqX), Random.Range(-freqY/2, freqY/2), 0));
        }
        Vector3 rightPos = cam.WorldToViewportPoint(n.nodes[n.nodes.Count-1]);
        if (rightPos.x < 1)
        {
            rightAdd(new Vector3(n.nodes[n.nodes.Count-1].x + Random.Range(1, freqX), Random.Range(-freqY/2, freqY/2), 0));
        }
	}

    public void makeMesh(Vector3[] verts, int[] tris, Vector3[] norms, Vector2[] uvs)
    {
        Mesh mesh = new Mesh();
        mesh.vertices = verts;
        mesh.triangles = tris;
        mesh.normals = norms;
        mesh.uv = uvs;

        GameObject go = new GameObject();

        MeshFilter mf = (MeshFilter)go.AddComponent(typeof(MeshFilter));
        go.AddComponent(typeof(MeshRenderer));
        mf.mesh = mesh;
        mesh.RecalculateBounds();
        go.AddComponent(typeof(MeshCollider));

        //go.renderer.material.mainTexture = null;
        go.renderer.material.shader = Shader.Find("Self-Illumin/Bumped Diffuse");
        go.renderer.material.color = Color.red;
    }

    public void rightAdd(Vector3 node)
    {
        Debug.Log("I don't fucking know");
        Vector3[] verts = {
                                          new Vector3(node.x, node.y - n.brushWidth / 2, 0),
                                          new Vector3(n.nodes[n.nodes.Count-1].x, n.nodes[n.nodes.Count-1].y - n.brushWidth / 2, 0),
                                          new Vector3(n.nodes[n.nodes.Count-1].x, n.nodes[n.nodes.Count-1].y + n.brushWidth / 2, 0),
                                          new Vector3(node.x, node.y + n.brushWidth / 2, 0)
                                      };
        n.nodes.Add(node);
        makeMesh(verts, tris, norms, uvs);
    }

    public void leftAdd(Vector3 node)
    {
        Vector3[] verts = {
                                          new Vector3(n.nodes[0].x, n.nodes[0].y - n.brushWidth / 2, 0),
                                          new Vector3(node.x, node.y - n.brushWidth / 2, 0),
                                          new Vector3(node.x, node.y + n.brushWidth / 2, 0),
                                          new Vector3(n.nodes[0].x, n.nodes[0].y + n.brushWidth / 2, 0)
                                      };
        n.nodes.Insert(0, node);
        makeMesh(verts, tris, norms, uvs);
    }

    public void drawAllNodes()
    {
        for (int k = 0; k < n.nodes.Count - 1; k++)
        {
            if (k != n.nodes.Count - 1)
            {
                Vector3[] verts = {
                                          new Vector3(n.nodes[k+1].x, n.nodes[k+1].y - n.brushWidth / 2, 0),
                                          new Vector3(n.nodes[k].x, n.nodes[k].y - n.brushWidth / 2, 0),
                                          new Vector3(n.nodes[k].x, n.nodes[k].y + n.brushWidth / 2, 0),
                                          new Vector3(n.nodes[k+1].x, n.nodes[k+1].y + n.brushWidth / 2, 0)
                                      };
                makeMesh(verts, tris, norms, uvs);
            }
        }
    }
}
