using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

[CustomEditor(typeof(Nodes))]
public class DrawGround : Editor
{
    Nodes n = null;
    Vector2[] uvs = new Vector2[]
					{
			            new Vector2(0, 0),
                        new Vector2(1, 0),
                        new Vector2(1, 1),
                        new Vector2(0, 1)
					};
    Vector3[] norms = new Vector3[]{
                  Vector3.forward,
                  Vector3.forward,
                  Vector3.forward,
                  Vector3.back
            };

    int[] tris = new int[6] { 0, 1, 3, 1, 2, 3 };

    public bool isDrawing = false;
    public override void OnInspectorGUI()
    {
        //HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

        DrawDefaultInspector();

        n = this.target as Nodes;

        isDrawing = (bool)GUILayout.Toggle(isDrawing, "Draw Ground(Or don't. Fuck you, Ron)");

        //Event currentEvent = Event.current;

        if(GUILayout.Button("Clear Nodes"))
        {
            n.nodes.Clear();
        }
        if (GUILayout.Button("Delete Start"))
        {
            n.nodes.RemoveAt(0);
        }
        if (GUILayout.Button("Delete End"))
        {
            n.nodes.RemoveAt(n.nodes.Count-1);
        }
        if (GUILayout.Button("Order Nodes"))
        {
            float max = float.MinValue;
            int index = 0;
            for (int k = n.nodes.Count-1; k >= 0; k--)
            {
                for (int j = 0; j <= k; j++)
                {
                    if (n.nodes[j].x > max)
                    {
                        max = n.nodes[j].x;
                        index = j;                                               
                    }
                }
                max = float.MinValue;
                Vector3 temp = n.nodes[index];
                n.nodes[index] = n.nodes[k];
                n.nodes[k] = temp;
            }
        }
       
        if (GUILayout.Button("Draw Mesh"))
        {
            drawAllNodes();
        }  
        #region the reason why I stay awake at night crying
        /*if (isDrawing&&currentEvent.Equals(Event.KeyboardEvent("N")))
        {
                //currentEvent.Use();
                n.TroubleShoot++;
                //Ray woldRay = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                Ray worldRay = SceneView.lastActiveSceneView.camera.ViewportPointToRay(currentEvent.mousePosition);
                Physics.Raycast(worldRay);
                RaycastHit hit;
                
                n.nodes.Add(new Vector3(hit.point.x, hit.point.y, 0));
        }*/
        #endregion
    }
    public void makeMesh(Vector3[] verts, int[] tris, Vector3[] norms, Vector2[] uvs)
    {
        Mesh mesh = new Mesh();
        mesh.vertices = verts;
        mesh.triangles = tris;
        mesh.normals = norms;
        mesh.uv = uvs;

        GameObject go = new GameObject();

        MeshFilter mf = (MeshFilter)go.AddComponent(typeof(MeshFilter));
        go.AddComponent(typeof(MeshRenderer));
        mf.mesh = mesh;
        mesh.RecalculateBounds();
        go.AddComponent(typeof(MeshCollider));

        //go.renderer.material.mainTexture = null;
        go.renderer.material.shader = Shader.Find("Self-Illumin/Bumped Diffuse");
        go.renderer.material.color = Color.red;

        go.AddComponent(typeof(Rigidbody));
    }

    public void rightAdd(Vector3 node)
    {
        Debug.Log("I don't fucking know");
        Vector3[] verts = {
                                          new Vector3(node.x, node.y - n.brushWidth / 2, 0),
                                          new Vector3(n.nodes[n.nodes.Count-1].x, n.nodes[n.nodes.Count-1].y - n.brushWidth / 2, 0),
                                          new Vector3(n.nodes[n.nodes.Count-1].x, n.nodes[n.nodes.Count-1].y + n.brushWidth / 2, 0),
                                          new Vector3(node.x, node.y + n.brushWidth / 2, 0)
                                      };
        n.nodes.Add(node);
        makeMesh(verts, tris, norms, uvs);
    }

    public void leftAdd(Vector3 node)
    {
        Vector3[] verts = {
                                          new Vector3(n.nodes[0].x, n.nodes[0].y - n.brushWidth / 2, 0),
                                          new Vector3(node.x, node.y - n.brushWidth / 2, 0),
                                          new Vector3(node.x, node.y + n.brushWidth / 2, 0),
                                          new Vector3(n.nodes[0].x, n.nodes[0].y + n.brushWidth / 2, 0)
                                      };
        n.nodes.Insert(0, node);
        makeMesh(verts, tris, norms, uvs);
    }

    public void drawAllNodes()
    {
        for (int k = 0; k < n.nodes.Count - 1; k++)
        {
            if (k != n.nodes.Count - 1)
            {
                Vector3[] verts = {
                                          new Vector3(n.nodes[k+1].x, n.nodes[k+1].y - n.brushWidth / 2, 0),
                                          new Vector3(n.nodes[k].x, n.nodes[k].y - n.brushWidth / 2, 0),
                                          new Vector3(n.nodes[k].x, n.nodes[k].y + n.brushWidth / 2, 0),
                                          new Vector3(n.nodes[k+1].x, n.nodes[k+1].y + n.brushWidth / 2, 0)
                                      };
                makeMesh(verts, tris, norms, uvs);
            }
        }
    }
}
