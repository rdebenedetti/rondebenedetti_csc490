using UnityEngine;
using System.Collections;

public class SpawnEnemy : MonoBehaviour {
    public GameObject spawnThis;
    public float freqMin, freqMax;
    bool spawning;
    Square square = new Square();
	void Update () {
        if (!spawning)
            StartCoroutine(Spawn(spawnCooldown()));
	}

    IEnumerator Spawn(float cooldown)
    {
        spawning = true;
        yield return new WaitForSeconds(cooldown);
        GameObject enemy = (GameObject)Instantiate(spawnThis);
        enemy.rigidbody.position = rigidbody.position;
        square = enemy.GetComponentInChildren<Square>();
        square.UpdateMesh();
        spawning = false;
    }


    private float spawnCooldown()
    {
        return Random.Range(freqMin, freqMax);
    }

}
