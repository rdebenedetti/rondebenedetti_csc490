using UnityEngine;
using System.Collections;

public class EraserAI : MonoBehaviour {
    public float jump;
    public GameObject enemy;
    public float wait = 2;
    bool grounded = false;
    bool preparing = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (grounded&&!preparing)
        {
            StartCoroutine(Attack(wait));
        }
        
	}

    void OnCollisionStay(Collision collision)
    {
        if (collision != null && !preparing)
        {
            grounded = true;
        }

    }

    IEnumerator Attack(float cooldown)
    {
        gameObject.tag = "Expo";
        preparing = true;
        rigidbody.velocity = new Vector3(0, Mathf.Sqrt(2 * jump * -Physics.gravity.y), 0);
        enemy.animation.Play();
        yield return new WaitForSeconds(cooldown);
        grounded = false;
        preparing = false;
        gameObject.tag = "shake";
    }

}
