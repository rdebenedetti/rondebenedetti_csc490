using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
public class CharacterMotor : MonoBehaviour
{
    public GameObject gameControler;
    public float speed = 1.0f;
    public float maxSpeedChange = 1.0f;
    public float jumpHeight = 1.0f;
    public float airMovement = 0.1f;
    public float bump = 2;
    public TextMesh kills;
    [HideInInspector]
    public float direction;
    [HideInInspector]
    public bool shouldJump = false;
    [HideInInspector]
    public Vector3 targetVelocity = Vector3.zero;
    [HideInInspector]
    public GameControler gc;
    bool grounded = false;
	
	public int neededToWin;
    void Start()
    {
        gc = (GameControler)gameControler.GetComponent(typeof(GameControler));
    }
    void FixedUpdate()
    {
        Vector3 velocity = rigidbody.velocity;
        Vector3 velocityChange = (targetVelocity - velocity);
        velocityChange = velocityChange.normalized * Mathf.Clamp(velocityChange.magnitude, -maxSpeedChange, maxSpeedChange);
        velocityChange.y = 0;

        if (shouldJump && grounded)
        {
            rigidbody.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
            rigidbody.AddForce(bump * direction, 0, 0); 
        }
        rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);
        grounded = false;
    }

    void OnCollisionStay(Collision collision)
    {
        if (collision != null)
        {
            grounded = true;
                foreach (ContactPoint cp in collision.contacts)
                {                   
                    direction = cp.normal.x;
                }
              
        }
        
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Expo"))
        {
            foreach (ContactPoint cp in collision.contacts)
            {
                if (cp.normal.y > 0.5)
                {
                    Destroy(collision.gameObject);
                    kills.text += "|";
					if(kills.text.Length >= neededToWin)
						Application.LoadLevel("win");
                }
                else if (cp.normal.y < -0.5)
                {
                    gc.hit();
                }
            }
        }
        else if (collision.gameObject.tag.Equals("shake"))
        {
            foreach (ContactPoint cp in collision.contacts)
            {
                if (cp.normal.y > 0.5)
                {
                    Destroy(collision.gameObject);
                    kills.text += "|";
					if(kills.text.Length >= neededToWin)
						Application.LoadLevel("win");
                }
                else
                {
                    gc.hit();
                }
            }
        }
    }
    float CalculateJumpVerticalSpeed()
    {
        return Mathf.Sqrt(2 * jumpHeight * -Physics.gravity.y);
    }

    
}