using UnityEngine;
using System.Collections;

public class Listener : MonoBehaviour {

    private GameObject red, blue;
    private TextMesh textMesh, txtLives, txtScore, txtRedLeft, txtBlueLeft, txtHighScore;
    private int lives, score;
    private int redLeft = 44;
    private int blueLeft = 44;
    private float startTime, deltaT;
    private float timer = float.MaxValue;
    private GameObject magicTimer;
    private bool canTeleport = true;
    private Ball rBall, bBall;
   
        void Start () {
        red = GameObject.FindGameObjectWithTag("red");
        blue = GameObject.FindGameObjectWithTag("blue");

        rBall = (Ball)red.GetComponent("Ball");
        bBall = (Ball)blue.GetComponent("Ball");
        Physics.IgnoreCollision(red.collider, blue.collider);

        magicTimer = GameObject.Find("MagicTimer");
        textMesh = (TextMesh) magicTimer.GetComponent(typeof(TextMesh));

        lives = PlayerPrefs.GetInt("livesLeft");
        score = PlayerPrefs.GetInt("currentScore");

        txtScore = (TextMesh)GameObject.Find("Score").GetComponent(typeof(TextMesh));
        txtLives = (TextMesh)GameObject.Find("Lives").GetComponent(typeof(TextMesh));
        txtRedLeft = (TextMesh)GameObject.Find("RedLeft").GetComponent(typeof(TextMesh));
        txtBlueLeft = (TextMesh)GameObject.Find("BlueLeft").GetComponent(typeof(TextMesh));
        txtHighScore = (TextMesh)GameObject.Find("HighScore").GetComponent(typeof(TextMesh));

        txtHighScore.text = PlayerPrefs.GetInt("brickScore").ToString();
        txtScore.text = "Score: " + score.ToString();
        txtLives.text = "Lives: " + lives.ToString();
        }
        

        void Update () {

        
        if (canTeleport && bBall.IsFree() && rBall.IsFree())
        {
            if (Input.GetKeyUp(KeyCode.UpArrow))//red to blue
            {
                Vector3 v = blue.rigidbody.velocity;

                red.rigidbody.position = blue.rigidbody.position;
                GameObject.FindGameObjectWithTag("flash").rigidbody.position = blue.rigidbody.position;
                GameObject.FindGameObjectWithTag("flash").particleSystem.Play();
                if (v.y < 1 && v.y > -1)//stuck horizontal
                {
                    red.rigidbody.velocity = new Vector3(-v.x, v.y + 5, v.z);
                    blue.rigidbody.velocity = new Vector3(v.x, v.y + 5, v.z);
                }
                else
                {
                    red.rigidbody.velocity = new Vector3(-v.x, v.y * 1.1f, v.z);
                    blue.rigidbody.velocity = new Vector3(v.x, v.y * 1.1f, v.z);
                }
                TimerStart();

                canTeleport = false;

                print("blue.velocity.y: " + v.y + " velocity.x: " + v.x);
            }

            if (Input.GetKeyUp(KeyCode.DownArrow))//blue to red
            {
                Vector3 v = red.rigidbody.velocity;

                blue.rigidbody.position = red.rigidbody.position;
                GameObject.FindGameObjectWithTag("flash").rigidbody.position = blue.rigidbody.position;
                GameObject.FindGameObjectWithTag("flash").particleSystem.Play();

                if (v.y < 1 && v.y > -1)//stuck horizontal
                {
                    red.rigidbody.velocity = new Vector3(-v.x, v.y + 5, v.z);
                    blue.rigidbody.velocity = new Vector3(v.x, v.y + 5, v.z);
                }
                else
                {
                    red.rigidbody.velocity = new Vector3(-v.x, v.y * 1.1f, v.z);
                    blue.rigidbody.velocity = new Vector3(v.x, v.y * 1.1f, v.z);
                }

                TimerStart();

                canTeleport = false;
            }
        }

        startTime = Time.time;
        deltaT = startTime - timer;

        switch ((int)deltaT)
        {
            case 0:
                textMesh.text = "3";
                magicTimer.animation.Play();
                break;
            case 1:
                 textMesh.text = "2";
                magicTimer.animation.Play();
                break;
            case 2:
                textMesh.text = "1";
                magicTimer.animation.Play();
                break;
            default:
                textMesh.text = "";
                canTeleport = true;
                break;
        }
        
        }

    public void TimerStart()
    {
        timer = Time.time;
    }

    public void RedDead()
    {
        redLeft--;
        txtRedLeft.text = "Red Left: " + redLeft.ToString();
        if (blueLeft == 0 && redLeft == 0)
        {
            win();
        }
    }

    public void BlueDead()
    {
        blueLeft--;
        txtBlueLeft.text = "Blue Left: " + blueLeft.ToString();
        if (blueLeft == 0 && redLeft == 0)
        {
            win();
        }
    }

    public void Scored()
    {
        score++;
        txtScore.text = "Score: " + score.ToString();
    }

    public void Died()
    {
        lives--;
        txtLives.text = "Lives: " + lives.ToString();
        if (lives == 0)
        {
            GameOver();
        }
    }
    public void win()
    {

        PlayerPrefs.SetInt("livesLeft", lives++);
        PlayerPrefs.SetInt("currentScore", score);
        Application.LoadLevel("BrickBreak");
    }

    public void GameOver()
    {
        int hack = PlayerPrefs.GetInt("brickScore");//for some reason putting it in the if statement makes it always evaluate to true
        if (score > hack)
        {
            print("brickScore: " + PlayerPrefs.GetInt("brickScore") + " Score: " + score);
            PlayerPrefs.SetInt("brickScore", score);
            txtHighScore.text = score.ToString();
        }
        PlayerPrefs.SetInt("livesLeft", 3);
        PlayerPrefs.SetInt("currentScore", 0);
        redLeft = 44;
        blueLeft = 44;
        Application.LoadLevel("BrickBreak");
    }
    public void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("livesLeft", 3);
        PlayerPrefs.SetInt("currentScore", 0);
    }
}


