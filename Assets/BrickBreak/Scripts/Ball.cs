using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

    public float speed = 40;
    private Vector3 v;
    private bool free;
    private GameObject paddle, redExplosion, blueExplosion;
    public Listener ls;
    void Start()
    {
        ls = (Listener)GameObject.Find("Listener").GetComponent("Listener");
        paddle = GameObject.FindGameObjectWithTag("Player");
        redExplosion = GameObject.Find("redExplosion");
        blueExplosion = GameObject.Find("blueExplosion");
    }

	void Update () {


        if (free)
        {
            Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
            Vector3 bottomLeft = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);

            // ball hits the edges of the windows
            if (topRight.y > 1)
            {
                rigidbody.velocity = new Vector3(v.x, -Mathf.Abs(v.y), v.z);
            }
            else if (bottomLeft.x > 1)
            {
                rigidbody.velocity = new Vector3(-Mathf.Abs(v.x), v.y, v.z);
            }
            else if (topRight.x < 0)
            {
                rigidbody.velocity = new Vector3(Mathf.Abs(v.x), v.y, v.z);
            }
            else if (bottomLeft.y < 0)
            {
                free = false;
                if (tag.Equals("red"))
                {
                    redExplosion.rigidbody.position = rigidbody.position;
                    redExplosion.particleSystem.Play();
                    Vector3 p = paddle.rigidbody.position;
                    rigidbody.position = new Vector3(p.x - 4.5f, p.y + 5f, p.z);
                    ls.Died();
                }
                else
                {
                    blueExplosion.rigidbody.position = rigidbody.position;
                    blueExplosion.particleSystem.Play();
                    Vector3 p = paddle.rigidbody.position;
                    rigidbody.position = new Vector3(p.x + 4.5f, p.y + 5f, p.z);
                    ls.Died();
                }
            }
        }
        else
        {
            rigidbody.velocity = paddle.rigidbody.velocity;
        }

        if (Input.GetKeyUp(KeyCode.Space) && free == false)
        {
            rigidbody.velocity = Vector3.down * speed;
            free = true;
        }
	}

    void FixedUpdate()
    {
        if (free)
        {
            v = rigidbody.velocity;
            float magnitude = rigidbody.velocity.magnitude;
            if (magnitude > speed)
            {
                float prop = (magnitude - speed) / 4;
                rigidbody.AddForce(new Vector3(-v.x, -v.y, 0) * prop);
            }
            else if (magnitude < (speed - (speed * 0.04)))
            {
                rigidbody.AddForce(new Vector3(v.x, v.y, 0));
            }
        }
    }

    void OnCollisionExit(Collision collision){

        if(collision.gameObject.tag.Equals("Player")){
            float x = (transform.position.x - paddle.transform.position.x)* 10;
            float y = Mathf.Abs(transform.position.y - paddle.transform.position.y);
        
            rigidbody.AddForce(new Vector3(x,y,0)* speed);
        }
}
    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag.Equals("BlueBrick")  && gameObject.tag.Equals("blue"))
        {
            Destroy(collision.gameObject);
            ls.Scored();
            ls.BlueDead();
        }
        else if (collision.gameObject.tag.Equals("RedBrick") && gameObject.tag.Equals("red"))
        {
            Destroy(collision.gameObject);
            ls.Scored();
            ls.RedDead();
        }
    }
    public bool IsFree()
    {
        return free;
    }
}

