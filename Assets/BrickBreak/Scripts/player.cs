using UnityEngine;
using System.Collections;

public class player : MonoBehaviour {

	 public float speed = 50;
        
        void Update () {
        
        Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
        Vector3 bottomLeft = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
        
                if(Input.GetAxis("Horizontal") < 0){//going left
                        if(bottomLeft.x >= 0){
                                rigidbody.velocity = Vector3.right * Input.GetAxis("Horizontal") * speed;       
                        }else{
                                rigidbody.velocity = Vector3.zero;      
                        }
                }else{//going right
                        if(topRight.x <= 1){
                                rigidbody.velocity = Vector3.right * Input.GetAxis("Horizontal") * speed;       
                        }else{
                                rigidbody.velocity = Vector3.zero;      
                        }
                } 
        }
}
